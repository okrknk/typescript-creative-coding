
# TypeScript fullscreen canvas boilerplate

This is a fork of [typescript-webpack-minimal](https://github.com/oskarkunik/typescript-webpack-minimal) by [jonaskello](https://github.com/jonaskello).

The code in `src/canvas.ts` is a minimal TypeScript version of full-screen canvas JavaScript function described by George Gally [here](https://hackernoon.com/creative-coding-basics-4d623af1c647).

# How to use

```
yarn install
yarn start
```