'use strict';

import { CanvasInit } from './canvas';

let canvasInit = new CanvasInit();

canvasInit.createCanvas('canvas');
