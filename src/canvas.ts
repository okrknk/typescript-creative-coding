'use strict';

export class CanvasInit {
    private body: HTMLBodyElement = document.querySelector('body');
    
    public createCanvas(canvas_name: string):CanvasRenderingContext2D  {
        const canvas: HTMLCanvasElement = document.createElement('canvas');
        const ctx: CanvasRenderingContext2D = canvas.getContext('2d');
    
        canvas.setAttribute('id', canvas_name);
        canvas.style.position = 'absolute';
        canvas.style.left = '0px';
        canvas.style.top = '0px';
    
        this.body.appendChild(canvas);
        this.resize();
        window.addEventListener('resize', this.resize, false);
        
        return ctx;
    }
    
    
    private resize(): void {
        const c = document.getElementsByTagName('canvas');
        const width = window.innerWidth;
        const height = window.innerHeight;
        for (var i = 0; i < c.length; i++) {
            c[i].width = width;
            c[i].height = height;
        }
        console.log(`resize: ${width}x${height}`);
    }
}
